import React, { Component } from 'react'
import axios from "./axios"
import { Link } from "react-router-dom"
import Spinner from "./Spinner"
export default class Books extends Component {

    state = {
        books: [],
        loading: false,
        error: null
    }
    componentDidMount = () => {
        this.setState({ loading: true });
        // document.cookie = "limit=8; expires="+new Date(Date.now() + 360*24*60*60*1000)
        axios.get("/books?limit=50")
            .then(res => {
                console.log("data", res)
                this.setState({ books: res.data.data, loading: false })
            })
            .catch(err => {
                console.log("err", err)
                this.setState({ loading: false, error: err.response })
            })
    }
    render() {
        return (
            <div>
                <h1 className="title">Амазон номын дэлгүүр</h1>
                {this.state.loading ? <Spinner></Spinner> : <div className="columns is-multiline">

                    {this.state.books.map(el => <div className="column is-one-quarter">
                        <Link to={`/books/${el._id}`}><img src={`https://data.internom.mn/media/images/${el.photo}`}></img></Link></div>)}
                </div>}
            </div >
        )
    }
}
