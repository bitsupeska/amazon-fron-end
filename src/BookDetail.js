import React, { Component } from 'react'
import axios from "./axios"
import Spinner from "./Spinner"
export default class BookDetail extends Component {
    state = {
        name: null,
        price: null,
        content: null,
        photo: null,
        error: null,
        success: null,
        loading: false,
        deleted: null,
    }

    componentDidMount = () => {
        this.setState({ loading: true })

        axios.get('/books/' + this.props.match.params.id)
            .then(res => {
                this.setState({ ...res.data.data, loading: false })
            })
            .catch(err => {
                console.log("err", err.reponse);
                this.setState({ loading: true, error: err.reponse.data.error.message })
            })
    }
    handleChange = (e) => {
        this.setState({ success: null, error: null })
        const { name, value } = e.target;
        this.setState({ [name]: value, error: null })
    }

    goBack = () => {
        this.props.history.goBack();
    }
    handleSave = () => {
        this.setState({ loading: true, success: null })
        const token = localStorage.getItem("token")
        console.log("token", token)
        axios.put('/books/' + this.props.match.params.id, {
            name: this.state.name,
            price: this.state.price,
            content: this.state.content
        })// {
            //     headers: {
            //         Authorization: `Bearer ${token}`
            //     },
            // }
            .then(res => {
                this.setState({ ...res.data.data, loading: false, success: "Амжилттай хадгалагдлаа" })
            })
            .catch(err => {
                console.log("err", err.response);
                this.setState({ loading: false, error: err.response.data.error.message, success: null })
            })
    }
    handleDelete = () => {
        this.setState({ loading: true, success: null })
        const token = localStorage.getItem("token")
        console.log("token", token)
        axios.delete('/books/' + this.props.match.params.id,)
            // {
            //     headers: {
            //         Authorization: `Bearer ${token}`
            //     },
            // }

            .then(res => {
                this.setState({ ...res.data.data, loading: false, deleted: true })
            })
            .catch(err => {
                this.setState({ loading: false, error: err.response.data.error.message, success: null })
            })
    }
    render() {
        if (this.state.deleted) {
            return <div className="notification is-danger">Амжилттай устгагдлаа</div>
        }
        return (
            <>
                {this.state.error && <div className="notification is-warning">{this.state.error}</div>}
                {this.state.success && <div className="notification is-success">{this.state.success}</div>}
                {this.state.loading ? <Spinner></Spinner> :
                    <> <h1 className="title">{this.state.name}</h1>
                        <div className="media">
                            <div className="media-left">
                                <img src={`https://data.internom.mn/media/images/${this.state.photo}`}></img>
                            </div>
                            <div className="media-content">
                                <div className="field">
                                    <label className="label">Нэр</label>
                                    <input className="input" name="name" value={this.state.name} onChange={this.handleChange} />
                                </div>
                                <div className="field">
                                    <label className="label">Үнэ</label>
                                    <input className="input" name="price" value={this.state.price} onChange={this.handleChange} />
                                </div>
                                <div className="field">
                                    <label className="label">Агуулга</label>
                                    <textarea style={{ height: "10em" }} className="input" name="content" value={this.state.content} onChange={this.handleChange} />
                                </div>
                                <div className="field">
                                    <button className="button is-success" onClick={this.goBack}>Буцах </button>
                                    &nbsp;
                                    <button className="button is-link" onClick={this.handleSave}>Хадгалах</button>
                                    &nbsp;
                                    <button className="button is-danger" onClick={this.handleDelete}>Устгах</button>
                                </div>
                            </div>
                        </div>
                    </>
                }
            </>
        )
    }
}
