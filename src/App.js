import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Login from "./Login"
import Books from "./Books"
import BookDetail from "./BookDetail"
import NavBar from "./NavBar"
import axios  from './axios'
export default class App extends Component {
    state = {
        token: null
    }
    handleLogin = (token) => {
        this.setState({ token });
        localStorage.setItem("token", token);
        this.router.history.push("/books")
    }
    handleLogout = () => {
        console.log("garlaa")
        localStorage.removeItem("token")
        this.setState({ token: null })
        // document.cookie = "amazon-token=null; expires="+new Date(Date.now() - 360*24*60*60*1000)
        axios.get("/users/logout").then(res=>{
            this.router.history.push("/")

        }).catch(ex=>{
            console.log("err",ex)
        })
    }
    render() {
        return (
            <Router ref={(router) => { this.router = router }}>
                <NavBar onLogout={this.handleLogout}></NavBar>
                <div className="container">
                    <Switch>
                        <Route exact path="/books" component={Books}></Route>
                        <Route path="/books/:id" component={BookDetail}></Route>
                        <Route path="/" render={() => <Login onLogin={this.handleLogin}></Login>}></Route>
                    </Switch>
                </div>
            </Router>
        )
    }
}
