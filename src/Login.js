import React, { Component } from 'react'
import axios from "./axios"
import Spinner from "./Spinner"
import { Redirect } from "react-router-dom"
export default class Login extends Component {
    state = {
        email: null,
        password: null,
        error: null,
        loading: false,
    }
    handleType = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value, error: null })
    }
    handleClick = () => {
        axios.post("users/login", {
            email: this.state.email,
            password: this.state.password
        }).then(res => {
            console.log("res", res)
            this.setState({ loading: false })
            this.props.onLogin(res.data.token);
        }).catch(err => {

            this.setState({ error: err.response.data.error.message, loading: false })
        })
    }
    render() {

        return (
            < div >
                {document.cookie && <Redirect to="/books" />}
                {this.state.error && <div className="notification is-warning">{this.state.error}</div>}
                <div className="field">
                    <label className="label">Имэйл</label>
                    <input className="input" name="email" type="text" onChange={this.handleType}></input>
                </div>
                <div className="field">
                    <label className="label">Нууц үг</label>
                    <input className="input" name="password" type="text" onChange={this.handleType}></input>
                </div> <div className="field">
                    {this.state.loading ? <Spinner></Spinner> : <button className="button is-link" onClick={this.handleClick}>Нэвтрэх</button>}
                </div>
            </div >
        )
    }
}
